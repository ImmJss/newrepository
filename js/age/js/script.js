"use strict";
/*
alert('Hello, world!');  функция
console.log('Hello, world!');  вывод в консоль, не для пользователей */

/*let a = 1;
let b = 2;
let c;
a = 4;
c = a;*/
/*типы данных: число, строка, null, undefined (неопределенность значений переменных), infiniti (математическая бесконечность), объект, boolean
let string = 'I am string';
console.log(string);
let bool = true;
console.log(bool);
let n = null;
console.log(n);
let u = undefined;
console.log(u);*/

/*let value = 2;
alert(typeof value);*/
/*let isSunny = confirm('Сегодня солнечно');
console.log(isSunny);
console.log(typeof isSunny);*/
/*let date = prompt('Введите дату'); окно для сбора
console.log(date);
console.log(typeof date);*/
/*let
class
function
return нельзя использовать в качестве названия переменных*/

/*const DATE = '9.05.2021'
alert(DATE);если вычисления то название константы вводится маленькими буквами*/

/*let productName;*/
/*let product_name;*/

/*let value = true;
alert(typeof value);
console.log(value);
value = Number(value);
alert(typeof value);
console.log(value);*/

/*let str = '123';
alert(typeof str);
let num = Number(str);
alert(typeof num);*/

/*let age = Number("Любая строка вместо числа");
alert(age);
alert(Number("123z"));
alert(parseInt("123z"));*/

/*let two  = "2";
let three = "3";
alert(Number(two) + Number(three));*/

/*alert(parseInt(1.7));
alert(parseFloat(1.2));*/

/*alert(Boolean(0));
alert(Boolean(45));
alert(Boolean(""));
alert(Boolean("0"));
alert(Boolean(" "));*/

/*let name = prompt("Введите Ваше имя");
alert("Ваше имя " + name);*/

/*alert(8%2); деление без остатка
2**2 возведение в степень
alert(4**(1/2));*/

/*let a = "2";
let b = "3";
let aNum = -a + -b;
console.log(aNum);*/

/*let a = 1;
let b = 2;
let c = 4 * b - (a = b + 1);
alert(a);
alert(c);
a = b = c = 2 + 2;*/

/*let counter = 1; инкемент, декремент
let a = counter++;
alert(a);*/

/*let a = 1; сокращенная запись
a += 1;
alert(a);*/

/*let a = 0;
let b = "0";
alert(a === b); тройное равно приводит к строгому сравнению, двойное сравнение не строгое сравнениее*/

/*a != b обратное равенство, a!==b строгое сравнение*/

/*!a дает противоположное значение. 
let a = 123;
console.log(!a); false
console.log(typeof !a); boolean*/

/*let a = prompt("Введите первое число");
let b = prompt("Введите второе число");
alert(Number(a) + Number(b));*/

/*let one = prompt("Введите первое число")
let two = prompt("Введите второе число");
let summa = Number(one) + Number(two);

if(summa > 0 && summa < 10){
	alert('Сумма меньше 10');
}
else if(summa >= 10 && summa < 50){
	alert('Сумма от 10 до 50');
}
else if(summa >=50){
	alert('Сумма больше 50');
}
else{
	alert('Число отрицательное');
}*/


let podpiska = confirm("Хотите оформить подписку?");
if(podpiska==true){
	let age = prompt("Сколько Вам лет");
	if(age >= 18){
		alert("Добро пожаловать");
	}
	else if(age >= 12){
		alert("Подписку оформить можно");
	}
	else if(age < 12){
		alert("Вам меньше 12 лет");
	}
}
else{
	alert("Всего доброго")
}


